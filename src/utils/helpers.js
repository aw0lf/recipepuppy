const sort = (prop, arr) => {
    prop = prop.split('.');
    const len = prop.length;

    arr.sort(function (a, b) {
        let i = 0;
        while( i < len ) {
            a = a[prop[i]];
            b = b[prop[i]];
            i++;
        }
        if (a < b) {
            return -1;
        } else if (a > b) {
            return 1;
        } else {
            return 0;
        }
    });
    return arr;
};

const fillArray = (length, step) => Array.from(Array(length), (x, i) => i + step);

export { sort, fillArray };
