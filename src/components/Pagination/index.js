import React, {useState, useEffect} from 'react'
import { fillArray } from "../../utils/helpers"
import  {PaginationWrapper, Li} from './style'

const Pagination = ({ onChangePage, page }) => {
    const [ pageNumbers, setPageNumbers ] = useState(fillArray(10,1));

    useEffect (() => {
        let updatedPageNumbers;
        if (page <= 5) {
            updatedPageNumbers = fillArray(10,1);
        } else if (page > 5) {
            updatedPageNumbers = fillArray(10, page - 4);
        }
        return setPageNumbers(updatedPageNumbers);
    },[page]);

    return <PaginationWrapper>
                {pageNumbers.map(number => {
                    return <Li
                        active = {number===page}
                        key={number}
                        id={number}
                        onClick={()=> onChangePage(number)}
                        >
                        {number}
                    </Li>
                })}
            </PaginationWrapper>
};
export default Pagination;
