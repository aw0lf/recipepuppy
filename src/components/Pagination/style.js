import styled, { css } from 'styled-components'

const PaginationWrapper = styled.ul`
    list-style: none;
    display: flex;

`;

const Li = styled.li`
    margin-right: 0.2em;
    color: #34421E;;
    user-select: none;
    cursor: pointer; 
    ${props => props.active && css`
    color: #76323F
    `}
`;

export { PaginationWrapper, Li };
