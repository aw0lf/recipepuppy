import React from 'react'

import useInputState from "../../hooks/useInputState"
import Button from "../Button"
import { sort } from "../../utils/helpers"
import {
    A,
    H4,
    Ingredient,
    Item,
    ListWrapper,
    Pizza,
    InputWrapper
} from "./style"

const List = ({ setSearch, queries, items }) => {
    const { value, onChange, onClick } = useInputState();
    sort('title', items);
    return <div>
            <form
                id='searchForm'
                onSubmit={event => {
                event.preventDefault();
                setSearch(value);
                }}
            >
            <InputWrapper
                onChange={onChange}
                value={value}
                placeholder='Enter the ingredients'
            />
            <Button
                type='submit'
                form='searchForm'
                value='Search'
                text='Search'
            />
            </form>
            <ListWrapper>
                {items.map((item, index) => {
                    return <Item
                            sky={index % 2 === 0}
                            key={index}
                            >
                            {item.thumbnail.length > 0 ?
                                <img
                                    src={item.thumbnail}
                                    alt={item.title}/>
                                :
                                <Pizza>🍕</Pizza>
                            }
                            <A href={item.href}>{item.title}</A>
                            <H4>ingredients:</H4>
                            {item.ingredients.split(', ').map((ingredient, index) => {
                                return <Ingredient
                                        key={ingredient+index}
                                        chosen={queries.includes(ingredient)}
                                        onClick={()=> {
                                        onClick(ingredient);
                                        setSearch(value+`,${ingredient}`);
                                        }}
                                        >
                                    {ingredient},
                                </Ingredient>
                            })}
                            </Item>
                    })
                }
            </ListWrapper>
        </div>
};
export default List;
