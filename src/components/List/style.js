import styled, { css } from 'styled-components'

const InputWrapper = styled.input`
    border:2px solid #C19434;
    font-size:1.75em;
    padding:.25em .5em .3125em;
    color: #C19434;
    border-radius:.25em;
    background:transparent;
    
    &:focus{
    outline:none;
    color:#C19434;
    border-color:#C19434;
    box-shadow:0 0 .25em #C19434,inset 0 0 .25em #C19434;
    }
    
    @media only screen and (max-width: 425px) {
        font-size:1.25em;
    }
`;

const Item = styled.div`
  margin: 0 1em;
  padding: 0.25em 1em;
  color: #34421E;
   
  & > img {
    padding: 0.25em;
    float: left;
    }
  ${props => props.sky && css`
  background: #F1F1EF;
  `}
`;

const A = styled.a`
    color:#C19434;
    margin: 0.25em;
    font-weight: bold;
`;
const ListWrapper = styled.div`

`;

const Pizza = styled.div`
    font-size:2.75em;
    float: left;
    margin-right: 1.4em;
    margin-bottom: 0.75em;
`;

const Ingredient = styled.a`
    cursor: pointer;
    ${props => props.chosen && css`
        color: #76323F;
    `}
`;

const H4 = styled.p`
    font-weight: bold;
`;

export {
    Item,
    ListWrapper,
    H4,
    A,
    Pizza,
    Ingredient,
    InputWrapper
};
