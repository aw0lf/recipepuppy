import React from 'react'
import ButtonWrapper from './style'

const Button = ({type, form, value, text}) => (
    <ButtonWrapper
        type={type}
        form={form}
        value={value}
    >
        {text}
    </ButtonWrapper >
);

export default Button;
