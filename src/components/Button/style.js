import styled, { css } from 'styled-components'

const ButtonWrapper = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid #C19434;
  color: #C19434;
  margin: 0.2em 0.75em;
  
  border:2px solid #C19434;
  font-size:1.50em;
  padding:.25em .5em .3125em;
  color: #C19434;
  border-radius:.25em;
  background:transparent;
  cursor: pointer;
  
  &:focus{
  outline:none;
  color:#C19434;
  border-color:#C19434;
  box-shadow:0 0 .25em #C19434,inset 0 0 .25em #C19434;
  }
  
  @media only screen and (max-width: 320px) {
        font-size:1em;
        padding:.25em 6.6em .3125em;
        margin: 0.4em 0.75em;
    }
   @media only screen and (max-width: 475px) {
        font-size:1em;
        padding:.25em 6.6em .3125em;
        margin: 0.4em 0.75em;
    }
`;
export default ButtonWrapper;
