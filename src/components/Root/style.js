import styled, { css } from 'styled-components'

const AppWrapper = styled.div`
    font-family: 'Open Sans', Helvetica, Arial, sans-serif;
`;

const Footer = styled.p`
     color: #34421E;
     & > a {
        text-decoration: none;
        color: #76323F;
     }
`;

export { AppWrapper, Footer };
