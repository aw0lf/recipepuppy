import React, { useState, useEffect } from 'react'

import { AppWrapper, Footer } from './style.js'
import List from '../List'
import useSearchState from "../../hooks/useSearchState"
import Pagination from "../Pagination"

const Root = () => {
    const { queries, addQueries } = useSearchState([]);
    const [items, setItems] = useState([]);
    const [page, setPage] = useState(1);
    const [url, setUrl] = useState('api/?');

    const setSearch = text => {
        const slicedText = text.split(',');
        const trimmedText = slicedText.map(item => item.trim());
        if (trimmedText.length > 0) {
            addQueries(trimmedText);
        }
    };

    const onChangePage = number => setPage(number);

    const formatUrl = () => {
        const q = `q=${queries[0]}`;
        let i = '';
        if (queries.length > 1) {
            i = `i=${queries.toString()}&`;
        }
        return `api/?${i}${q}`;
    };

    useEffect(() => setUrl(formatUrl()),[queries]);

    useEffect(() => setUrl(formatUrl() + `&p=${page}`),[page]);

    useEffect(() => {
        fetch(url)
            .then(res => res.json())
            .then((json => setItems(json.results)))
    }, [url]);

    return <AppWrapper>
            <List queries={queries} items={items} setSearch={setSearch}/>
            {items.length >= 1 && <Pagination page={page} onChangePage={onChangePage}/>}
            <Footer>Made with 🖤 for <a href='https://nozbe.com/'>Nozbe</a> by <a href='https://linkedin.com/in/ada-tycner'>Adrianna Tycne</a></Footer>
    </AppWrapper>
};

export default Root;
