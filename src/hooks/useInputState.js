import { useState } from 'react';

export default () => {
    const [value, setValue] = useState('');

    return {
        value,
        onChange: event => {
            setValue('');
            setValue(event.target.value);
        },
        onClick: ingredient => {
            setValue(value + ',' + ingredient);
        },
        onReset: () => setValue('')
    };
};
