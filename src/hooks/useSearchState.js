import { useState } from 'react'

export default initialValue => {
    const [queries, setQueries] = useState(initialValue);

    return {
        queries,
        addQueries: integrient => {
            setQueries(integrient);
        },
    }
};
