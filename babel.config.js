const presets = [
  '@babel/preset-react',
  ['@babel/preset-env', { modules: false }],
]

const plugins = []

module.exports = {
  presets,
  plugins,
}
